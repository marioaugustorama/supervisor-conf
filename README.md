# Instalação do supervisor

O [Supervisor](http://supervisord.org/) é um sistema cliente/servidor que 	
permite aos usuários monitorar e controlar os processos em servidores 
UNIX-like.

## Instalação do supervisor em Debian:

	apt-get install supervisor git

Após a instalação acesse o diretório /etc/supervisor/conf.d

Baixe os arquivos de acordo com o branch do cliente.

Faça os ajustes necessários nos arquivos .conf.

Para inicio automático juntamente com o boot do sistema.

	./update-rc.d supervisor enable

# Usando o supervisor.

Após a configuração do supervisor seu controle básico é feito por meio de linha
de comando.

	# supervisorctl
	gearman                          RUNNING    pid 6008, uptime 0:24:05
	worker                           RUNNING    pid 4210, uptime 1:01:14
	worker-src                       RUNNING    pid 4211, uptime 1:01:14

Comandos como start / stop / reload estão disponiveis para reiniciar os processo
pelo nome mostrado pelo comando acima.

	# supervisorctl stop gearman
	gearman: stopped

	# supervisorctl start gearman
	gearman: started

	# supervisorctl reload 
	Restarted supervisord


